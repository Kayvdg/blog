

Woensdag 6 september
====================

**Design Challenge (9.30 - 12.10)**
Tijdens de Design challenge van vandaag hebben we wat meer basis informatie gekregen over de opdrachten, hoe we ons moeten ziek melden en over hoe de workshops er uit komen te zien (de indeling). Dit was wel nodig, want sommige dingen waren nog onduidelijk en er was heel veel informatie gegeven in de eerste anderhalve week. Nu staat alles weer een beetje op een rijtje.

We hadden al vrij snel daarna een uurtje met onze studie coach. Hij heeft ons wat meer uitgelegd hoe je efficiënt kan samenwerken en over de verschillende rollen bij een vergadering. Ik heb de rol Cheerleader op mij genomen. Ik ben blij met mijn rol maar toch wil ik de rol van voorzitter beter leren kennen. In de loop van het jaar wil ik leren hoe je goed leiding kan geven aan een team zodat er een kwalitatief goed resultaat neergezet kan worden.  (was een leuke les btw! goede sfeer in de klas door de coach)

Na de pauze hebben we nog wat tijd gehad om te werken aan ons onderzoek. Ik heb deze grotendeels af gekregen, maar ben eerst verder gegaan met mijn moodboard over de doelgroep Social Work studenten. Deze had ik al snel af. Social Work studenten zijn meelevende en zorgende mensen die proberen iedereen te helpen.

Daarna hebben we een workshop Bloggen gekregen. Dit was nog best basic en makkelijk te begrijpen, ik wil hier graag meer van leren in de volgende les.