**Maandag 18 september**
-----------------------
Vandaag hebben we de eerste iteratie afgesloten met een presentatie. 
Als eerste hebben we onze deliverables gepresenteerd aan een ander groepje uit de klas. Hier hebben we wat feedback op gekregen:
 - leuk concept
 - Leuk dat het een bord is en dat je het gewoon in het lokaal kan spelen.
 - goed dat alle onderzoeken verwerkt zijn
 - duidelijke moodboards
 - Filmpje is leuke toevoeging voor onderzoek.

We moesten ons concept presenteren aan de hele klas. de presentatie moest 5 minuten duren en we hadden 15 minuten te tijd om deze voor te bereiden. Het was toch wel heel fijn dat we eindelijk het concept aan de hele klas konden laten zien, hierdoor krijg je van iedereen feedback en niet alleen van een beperkt aantal klasgenoten.
De feedback was:
 - Duidelijk gepraat, maar veel met rug naar publiek gedraaid.
 - Minigames is wel een leuke toevoeging als extra.
 - Duidelijker de kaarten uitleggen & wat de icons betekenen.
 - Beter nadenken over aantal spelers.
 - spelregels zijn wel helder
 - spel testen door andere mensen die het spel niet kennen.
 
Vandaag is de ook de start van de nieuwe iteratie, 'Kill Your Darlings'. Wel even wennen dat je je idee, waar je veel tijd in hebt gestopt, opzei moet zetten. Ik snap wel dat dit nou eenmaal moet. Soms staar je je zo dood op een goed idee, dat je je niet meer verdiept op andere goede ideeën. Er kwamen wel aardig wat vragen door de nieuwe briefing. Het was niet helemaal duidelijk wat er nou individueel of gezamenlijk ingeleverd moet worden. We hebben een begin gemaakt aan onze planning en taakverdeling, wat goed te doen is. 